.. Example Documentation Project documentation master file, created by
   sphinx-quickstart on Sat Aug 26 17:27:18 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Example Documentation Project's documentation!
=========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   sphinx
   restructuredtext
   i18n
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
